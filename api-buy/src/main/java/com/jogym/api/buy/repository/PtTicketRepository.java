package com.jogym.api.buy.repository;

import com.jogym.api.buy.entity.PtTicket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketRepository extends JpaRepository<PtTicket, Long> {

}
