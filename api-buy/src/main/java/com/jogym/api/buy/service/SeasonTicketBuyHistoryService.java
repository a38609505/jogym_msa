package com.jogym.api.buy.service;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.entity.SeasonTicket;
import com.jogym.api.buy.entity.SeasonTicketBuyHistory;
import com.jogym.api.buy.entity.StoreMember;
import com.jogym.api.buy.model.seasonticketbuyhistory.SeasonTicketBuyHistoryItem;
import com.jogym.api.buy.repository.SeasonTicketBuyHistoryRepository;
import com.jogym.common.enums.BuyStatus;
import com.jogym.common.exception.CAlreadyUsingSeasonTicketException;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SeasonTicketBuyHistoryService {
    private final SeasonTicketBuyHistoryRepository seasonTicketBuyHistoryRepository;

    public void setSeasonTicketBuyHistory(Customer customer, StoreMember storeMember, SeasonTicket seasonTicket) {
        if (!storeMember.equals(customer.getStoreMember()))
            throw new CMissingDataException(); // 회원이 체육관의 회원이 아닐 경우.
        if (!storeMember.equals(seasonTicket.getStoreMember()))
            throw new CMissingDataException(); // 해당 체육관에 맞지 않는 정기권일 경우.
        if (!isNewTicketOfMember(customer))
            throw new CAlreadyUsingSeasonTicketException(); // 이미 정기권을 갖고 있으면.

        SeasonTicketBuyHistory seasonTicketBuyHistory = new SeasonTicketBuyHistory.SeasonTicketBuyHistoryBuilder(customer, seasonTicket).build();

        seasonTicketBuyHistoryRepository.save(seasonTicketBuyHistory);
    }

    public void putHistoryDateLast(StoreMember storeMember, long historyId) {
        SeasonTicketBuyHistory data = seasonTicketBuyHistoryRepository.findByCustomer_StoreMemberAndIdAndBuyStatus(storeMember, historyId, BuyStatus.VALID).orElseThrow(CMissingDataException::new);
        data.putDateLast();

        seasonTicketBuyHistoryRepository.save(data);

    }

    public void putSeasonTicketBuyHistoryBuyStatus(StoreMember storeMember, long historyId, BuyStatus buyStatus) {
        SeasonTicketBuyHistory data = seasonTicketBuyHistoryRepository.findByCustomer_StoreMemberAndId(storeMember, historyId).orElseThrow(CMissingDataException::new);

        data.putBuyStatus(buyStatus);

        seasonTicketBuyHistoryRepository.save(data);
    }

    public ListResult<SeasonTicketBuyHistoryItem> getSeasonTicketBuyHistory(long memberId, StoreMember storeMember, int page) {
        Page<SeasonTicketBuyHistory> originData = seasonTicketBuyHistoryRepository.findAllByCustomer_IdAndCustomer_StoreMemberOrderByIdDesc(memberId, storeMember, ListConvertService.getPageable(page));

        List<SeasonTicketBuyHistoryItem> result = new LinkedList<>();

        originData.getContent().forEach(e -> {
            SeasonTicketBuyHistoryItem item = new SeasonTicketBuyHistoryItem.Builder(e).build();
            result.add(item);
        });
        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }

    private boolean isNewTicketOfMember(Customer customer) {
        long dupTicket = seasonTicketBuyHistoryRepository.countByCustomerAndBuyStatus(customer, BuyStatus.VALID);
        return dupTicket < 1;
    }
}

