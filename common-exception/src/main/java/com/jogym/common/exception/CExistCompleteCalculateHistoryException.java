package com.jogym.common.exception;

public class CExistCompleteCalculateHistoryException extends RuntimeException{
    public CExistCompleteCalculateHistoryException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExistCompleteCalculateHistoryException(String msg) {
        super(msg);
    }

    public CExistCompleteCalculateHistoryException() {
        super();
    }
}