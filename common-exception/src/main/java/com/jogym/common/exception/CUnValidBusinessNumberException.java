package com.jogym.common.exception;

public class CUnValidBusinessNumberException extends RuntimeException {
    public CUnValidBusinessNumberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUnValidBusinessNumberException(String msg) {
        super(msg);
    }

    public CUnValidBusinessNumberException() {
        super();
    }
}
