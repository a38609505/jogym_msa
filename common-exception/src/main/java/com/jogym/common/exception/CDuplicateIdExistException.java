package com.jogym.common.exception;

public class CDuplicateIdExistException extends RuntimeException{
    public CDuplicateIdExistException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDuplicateIdExistException(String msg) {
        super(msg);
    }

    public CDuplicateIdExistException() {
        super();
    }
}