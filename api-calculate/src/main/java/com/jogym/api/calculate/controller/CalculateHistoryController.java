package com.jogym.api.calculate.controller;

import com.jogym.api.calculate.entity.StoreMember;
import com.jogym.api.calculate.model.CalculateHistoryItem;
import com.jogym.api.calculate.model.CalculateHistoryRequest;
import com.jogym.api.calculate.model.CalculateHistoryResponse;
import com.jogym.api.calculate.model.CalculateHistoryUpdateRequest;
import com.jogym.api.calculate.service.storemember.ProfileService;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.model.SingleResult;
import com.jogym.api.calculate.service.calculateHistory.CalculateHistoryService;
import com.jogym.common.response.service.ResponseService;
import com.jogym.api.calculate.service.ptTicketBuyHistory.PtTicketBuyHistoryService;
import com.jogym.api.calculate.service.seasonticketbuyhistory.SeasonTicketBuyHistoryService;
import com.jogym.api.calculate.service.storemember.StoreMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "정산 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calculate")
public class CalculateHistoryController {
    private final PtTicketBuyHistoryService ptTicketBuyHistoryService;
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final CalculateHistoryService calculateHistoryService;
    private final StoreMemberService storeMemberService;
    private final ProfileService profileService;

    @ApiOperation(value = "정산내역 등록") // 관리자
    @PostMapping("/data/store-member/{storeMemberId}")
    public CommonResult setCalculateHistory(@PathVariable long storeMemberId, @RequestBody @Valid CalculateHistoryRequest calculateHistoryRequest){
        StoreMember storeMember = storeMemberService.setStore(storeMemberId);
        double ptPrice = ptTicketBuyHistoryService.totalPricePtTicketByYearMonth(storeMember,calculateHistoryRequest.getDateCreateYear(), calculateHistoryRequest.getDateCreateMonth());
        double seasonPrice = seasonTicketBuyHistoryService.totalPriceSeasonTicketByYearMonth(storeMember, calculateHistoryRequest.getDateCreateYear(), calculateHistoryRequest.getDateCreateMonth());
        calculateHistoryService.setCalculateHistory(storeMember, ptPrice, seasonPrice, calculateHistoryRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "정산내역 리스트 // 관리자") // 관리자
    @GetMapping("/list/store-member/{storeMemberId}")
    public ListResult<CalculateHistoryItem> getCalculateHistoryList(@PathVariable long storeMemberId, @RequestParam(value = "page", required = false, defaultValue = "1") int page){
        StoreMember storeMember = storeMemberService.setStore(storeMemberId);
        return ResponseService.getListResult(calculateHistoryService.getCalculateHistoryList(storeMember, page), true);
    }

    @ApiOperation(value = "정산내역 리스트 // 가맹점") // 가맹점
    @GetMapping("/list/store-member")
    public ListResult<CalculateHistoryItem> getCalculateHistoryList(@RequestParam(value = "page", required = false, defaultValue = "1") int page){
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getListResult(calculateHistoryService.getCalculateHistoryList(storeMember, page), true);
    }

    @ApiOperation(value = "정산내역 상세보기 // all") // all
    @GetMapping("/detail/history-id/{historyId}")
    public SingleResult<CalculateHistoryResponse> getCalculateHistoryDetail(@PathVariable long historyId){
        return ResponseService.getSingleResult(calculateHistoryService.getCalculateHistoryDetail(historyId));
    }

    @ApiOperation(value = "정산내역 수정 // 관리자") // 관리자
    @PutMapping("/calculate-history/{calculateHistoryId}")
    public CommonResult putCalculateHistory(@PathVariable long calculateHistoryId, @RequestBody @Valid CalculateHistoryUpdateRequest request){
        calculateHistoryService.putCalculateHistory(calculateHistoryId, request);

        return ResponseService.getSuccessResult();
    }
}
