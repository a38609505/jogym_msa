package com.jogym.api.calculate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCalculateApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiCalculateApplication.class, args);
    }
}
