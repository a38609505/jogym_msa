package com.jogym.api.calculate.repository;

import com.jogym.api.calculate.entity.PtTicketBuyHistory;
import com.jogym.api.calculate.entity.StoreMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface PtTicketBuyHistoryRepository extends JpaRepository<PtTicketBuyHistory, Long> {
    List<PtTicketBuyHistory> findAllByCustomer_StoreMemberAndDateCreateBetween(StoreMember storeMember, LocalDateTime startDate, LocalDateTime endDate);
}
