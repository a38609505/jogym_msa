package com.jogym.api.calculate.model;

import com.jogym.common.enums.CalculateStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CalculateHistoryUpdateRequest {

    @ApiModelProperty(notes = "등록년", required = true)
    @NotNull
    @Min(0)
    private Integer dateCreateYear;

    @ApiModelProperty(notes = "등록월", required = true)
    @NotNull
    @Min(1)
    @Max(12)
    private Integer dateCreateMonth;

    @ApiModelProperty(notes = "총매출", required = true)
    @NotNull
    private Double totalPrice;

    @ApiModelProperty(notes = "공제금", required = true)
    @NotNull
    private Double minusPrice;

    @ApiModelProperty(notes = "정산금", required = true)
    @NotNull
    private Double calculatePrice;

    @ApiModelProperty(notes = "정산 상태", required = true)
    @NotNull
    private CalculateStatus calculateStatus;

}
