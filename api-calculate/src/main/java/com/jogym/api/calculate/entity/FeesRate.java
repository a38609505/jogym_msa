package com.jogym.api.calculate.entity;

import com.jogym.api.calculate.model.FeesRate.FeesRateRequest;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FeesRate {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수정일시
    @Column(nullable = false)
    private LocalDateTime dateChange;

    // 수수료율
    @Column(nullable = false)
    private Double feesRate;

    // 수수료율을 0D로 해야할 지 기본 세금을 써야할 지 몰라 3.3을 적어 놓음.
    // 공무원처럼 수수료를 더 많이 떼야할 지 몰름.
    // 수정할 때 수정일 시를 현 일시로 교체
    // LocalDateTime.now로 해서 수정일 시를 현 일시로 바꾸어 봄.
    public void putFeesRate(FeesRateRequest request) {
        this.dateChange = LocalDateTime.now();
        this.feesRate = request.getFeesRate();
    }
    private FeesRate(FeesRateBuilder builder) {
        this.dateChange = builder.dateChange;
        this.feesRate = builder.feesRate;
    }

    public static class FeesRateBuilder implements CommonModelBuilder<FeesRate> {
        private final LocalDateTime dateChange;
        private final Double feesRate;

        public FeesRateBuilder(FeesRateRequest request) {
            this.dateChange = LocalDateTime.now();
            this.feesRate = request.getFeesRate();
        }

        @Override
        public FeesRate build() {
            return new FeesRate(this);
        }
    }
}
