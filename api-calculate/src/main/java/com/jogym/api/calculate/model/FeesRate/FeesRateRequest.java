package com.jogym.api.calculate.model.FeesRate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FeesRateRequest {

    @ApiModelProperty(notes = "수수료율", required = true)
    @Min(0)
    @Max(100)
    @NotNull
    private Double feesRate;
}
