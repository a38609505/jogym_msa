package com.jogym.api.calculate.repository;

import com.jogym.api.calculate.entity.CalculateHistory;
import com.jogym.api.calculate.entity.StoreMember;
import com.jogym.common.enums.CalculateStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CalculateHistoryRepository extends JpaRepository<CalculateHistory, Long> {

    Optional<CalculateHistory> findByStoreMemberAndDateCreateYearAndDateCreateMonthAndCalculateStatus(StoreMember storeMember, Integer year, Integer month, CalculateStatus calculateStatus);
    Page<CalculateHistory> findAllByStoreMemberOrderByIdDesc(StoreMember storeMember, Pageable pageable);
}
