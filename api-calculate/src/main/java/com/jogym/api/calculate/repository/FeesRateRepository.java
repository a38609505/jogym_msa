package com.jogym.api.calculate.repository;

import com.jogym.api.calculate.entity.FeesRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeesRateRepository extends JpaRepository<FeesRate, Long> {
}
