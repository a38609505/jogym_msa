package com.jogym.api.calculate.service.ptTicketBuyHistory;

import com.jogym.api.calculate.entity.PtTicketBuyHistory;
import com.jogym.api.calculate.entity.StoreMember;
import com.jogym.api.calculate.repository.PtTicketBuyHistoryRepository;
import com.jogym.common.function.CommonDate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PtTicketBuyHistoryService {
    private final PtTicketBuyHistoryRepository ptTicketBuyHistoryRepository;

    public Double totalPricePtTicketByYearMonth(StoreMember storeMember, int year, int month) {
        LocalDateTime startDateTime = LocalDateTime.of(year, month, 1, 0, 0, 0);
        LocalDateTime endDateTime = LocalDateTime.of(year, month, CommonDate.getLastDay(year, month), 23, 59, 59);
        double totalPrice = 0D;
        List<PtTicketBuyHistory> originList = ptTicketBuyHistoryRepository.findAllByCustomer_StoreMemberAndDateCreateBetween(storeMember, startDateTime, endDateTime);
        for (PtTicketBuyHistory item : originList) {
            totalPrice += (item.getPtTicket().getUnitPrice() * item.getPtTicket().getMaxCount());
        }
        return totalPrice;
    }

}
