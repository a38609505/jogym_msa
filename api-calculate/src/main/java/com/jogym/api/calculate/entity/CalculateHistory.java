package com.jogym.api.calculate.entity;

import com.jogym.common.enums.CalculateStatus;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.api.calculate.model.CalculateHistoryRequest;
import com.jogym.api.calculate.model.CalculateHistoryUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CalculateHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록년
    @Column(nullable = false)
    private Integer dateCreateYear;

    // 등록월
    @Column(nullable = false)
    private Integer dateCreateMonth;

    // 가맹점
    @JoinColumn(name = "storeMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 총 매출금
    @Column(nullable = false)
    private Double totalPrice;

    // 수수료율
    @Column(nullable = false)
    private Double feesRate;

    // 공제금
    @Column(nullable = false)
    private Double minusPrice;

    // 정산금
    @Column(nullable = false)
    private Double calculatePrice;

    // 정산 상태
    @Column(nullable = false, length = 1000)
    @Enumerated(EnumType.STRING)
    private CalculateStatus calculateStatus;

    public void putCalculateHistory(CalculateHistoryUpdateRequest request){
        this.dateCreateYear = request.getDateCreateYear();
        this.dateCreateMonth = request.getDateCreateMonth();
        this.totalPrice = request.getTotalPrice();
        this.minusPrice = request.getMinusPrice();
        this.calculatePrice = request.getCalculatePrice();
        this.calculateStatus = request.getCalculateStatus();
    }

    private CalculateHistory(CalculateHistoryBuilder builder) {
        this.dateCreateYear = builder.dateCreateYear;
        this.dateCreateMonth = builder.dateCreateMonth;
        this.storeMember = builder.storeMember;
        this.totalPrice = builder.totalPrice;
        this.feesRate = builder.feesRate;
        this.minusPrice = builder.minusPrice;
        this.calculatePrice = builder.calculatePrice;
        this.calculateStatus = builder.calculateStatus;
    }

    public static class CalculateHistoryBuilder implements CommonModelBuilder<CalculateHistory> {

        private final Integer dateCreateYear;
        private final Integer dateCreateMonth;
        private final StoreMember storeMember;
        private final Double totalPrice;
        private final Double feesRate;
        private final Double minusPrice;
        private final Double calculatePrice;
        private final CalculateStatus calculateStatus;

        public CalculateHistoryBuilder(StoreMember storeMember, Double totalPrice, CalculateHistoryRequest request) {
            this.dateCreateYear = request.getDateCreateYear();
            this.dateCreateMonth = request.getDateCreateMonth();
            this.storeMember = storeMember;
            this.totalPrice = totalPrice;
            this.feesRate = request.getFeesRate();
            this.minusPrice = totalPrice * request.getFeesRate() / 100 + 20000 > totalPrice ? 0D : totalPrice * feesRate / 100 + 20000;
            this.calculatePrice = totalPrice - minusPrice < 0 ? 0D : totalPrice - minusPrice;
            this.calculateStatus = CalculateStatus.ING;
        }

        @Override
        public CalculateHistory build() {
            return new CalculateHistory(this);
        }
    }

}