package com.jogym.api.calculate.entity;

import com.jogym.common.enums.TicketType;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
public class SeasonTicket {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @JoinColumn(name = "storeMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 구분 / 일 or 월
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private TicketType ticketType;

    // 정기권명
    @Column(nullable = false, length = 20)
    private String ticketName;

    // 최대월
    @Column(nullable = false)
    private Integer maxMonth;

    // 요금
    @Column(nullable = false)
    private Double unitPrice;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

}
