package com.jogym.api.calculate.model;

import com.jogym.api.calculate.entity.CalculateHistory;
import com.jogym.common.enums.CalculateStatus;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CalculateHistoryResponse {

    private Integer dateCreateYear;
    private Integer dateCreateMonth;
    private Double feesRate;
    private Double totalPrice;
    private Double minusPrice;
    private Double calculatePrice;
    private CalculateStatus calculateStatus;

    private CalculateHistoryResponse(Builder builder){
        this.dateCreateYear = builder.dateCreateYear;
        this.dateCreateMonth = builder.dateCreateMonth;
        this.totalPrice = builder.totalPrice;
        this.feesRate = builder.feesRate;
        this.minusPrice = builder.minusPrice;
        this.calculatePrice = builder.calculatePrice;
        this.calculateStatus = builder.calculateStatus;
    }
    public static class Builder implements CommonModelBuilder<CalculateHistoryResponse>{

        private final Integer dateCreateYear;
        private final Integer dateCreateMonth;
        private final Double totalPrice;
        private final Double feesRate;
        private final Double minusPrice;
        private final Double calculatePrice;
        private final CalculateStatus calculateStatus;

        public Builder(CalculateHistory calculateHistory){
            this.dateCreateYear = calculateHistory.getDateCreateYear();
            this.dateCreateMonth = calculateHistory.getDateCreateMonth();
            this.totalPrice = calculateHistory.getTotalPrice();
            this.feesRate = calculateHistory.getFeesRate();
            this.minusPrice = calculateHistory.getMinusPrice();
            this.calculatePrice = calculateHistory.getCalculatePrice();
            this.calculateStatus = calculateHistory.getCalculateStatus();
        }

        @Override
        public CalculateHistoryResponse build() {
            return new  CalculateHistoryResponse(this);
        }
    }
}
