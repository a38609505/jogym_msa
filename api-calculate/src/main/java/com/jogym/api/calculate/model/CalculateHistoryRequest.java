package com.jogym.api.calculate.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CalculateHistoryRequest {

    @ApiModelProperty(notes = "등록년", required = true)
    @NotNull
    private Integer dateCreateYear;

    @ApiModelProperty(notes = "등록월", required = true)
    @NotNull
    private Integer dateCreateMonth;

    @ApiModelProperty(notes = "수수료", required = true)
    @NotNull
    private Double feesRate;

}
