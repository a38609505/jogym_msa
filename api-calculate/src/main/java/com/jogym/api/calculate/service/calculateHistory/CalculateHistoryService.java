package com.jogym.api.calculate.service.calculateHistory;

import com.jogym.api.calculate.entity.CalculateHistory;
import com.jogym.api.calculate.entity.StoreMember;
import com.jogym.common.enums.CalculateStatus;
import com.jogym.common.exception.CExistCompleteCalculateHistoryException;
import com.jogym.common.exception.CExistIngCalculateHistoryException;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.api.calculate.model.CalculateHistoryItem;
import com.jogym.api.calculate.model.CalculateHistoryRequest;
import com.jogym.api.calculate.model.CalculateHistoryResponse;
import com.jogym.api.calculate.model.CalculateHistoryUpdateRequest;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.calculate.repository.CalculateHistoryRepository;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CalculateHistoryService {
    private final CalculateHistoryRepository calculateHistoryRepository;

    public void setCalculateHistory(StoreMember storeMember, Double ptPrice, Double seasonPrice, CalculateHistoryRequest request){
        checkDupCalculateHistory(storeMember, request.getDateCreateYear(), request.getDateCreateMonth());
        CalculateHistory calculateHistory = new CalculateHistory.CalculateHistoryBuilder(storeMember, ptPrice + seasonPrice, request).build();
        calculateHistoryRepository.save(calculateHistory);
    }

    public ListResult<CalculateHistoryItem> getCalculateHistoryList(StoreMember storeMember, int page){
        Page<CalculateHistory> originList = calculateHistoryRepository.findAllByStoreMemberOrderByIdDesc(storeMember, ListConvertService.getPageable(page));
        List<CalculateHistoryItem> result = new LinkedList<>();
        originList.getContent().forEach(e -> {
            CalculateHistoryItem item = new CalculateHistoryItem.Builder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public CalculateHistoryResponse getCalculateHistoryDetail(long historyId){
        CalculateHistory data = calculateHistoryRepository.findById(historyId).orElseThrow(CMissingDataException::new);

        return new CalculateHistoryResponse.Builder(data).build();
    }

    public void putCalculateHistory(long historyId, CalculateHistoryUpdateRequest request){
        CalculateHistory calculateHistory = calculateHistoryRepository.findById(historyId).orElseThrow(CMissingDataException::new);

        calculateHistory.putCalculateHistory(request);

        calculateHistoryRepository.save(calculateHistory);
    }

    private void checkDupCalculateHistory(StoreMember storeMember, int year, int month){
        Optional<CalculateHistory> isNewHistory = calculateHistoryRepository.findByStoreMemberAndDateCreateYearAndDateCreateMonthAndCalculateStatus(storeMember, year, month, CalculateStatus.ING);
        if (isNewHistory.isPresent()) throw new CExistIngCalculateHistoryException();
        Optional<CalculateHistory> existHistory = calculateHistoryRepository.findByStoreMemberAndDateCreateYearAndDateCreateMonthAndCalculateStatus(storeMember, year, month, CalculateStatus.COMPLETE);
        if (existHistory.isPresent()) throw new CExistCompleteCalculateHistoryException();
    }
}
