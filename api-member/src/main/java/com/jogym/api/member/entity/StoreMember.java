package com.jogym.api.member.entity;

import com.jogym.api.member.model.StoreMemberUpdateRequest;
import com.jogym.common.enums.MemberGroup;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.api.member.model.StoreMemberCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreMember implements UserDetails {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원가입일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 맴버그룹
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;

    // 아이디
    @Column(nullable = false, unique = true, length = 30)
    private String username;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 연락처
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    // 사업자 등록번호
    @Column(nullable = false, length = 12, unique = true)
    private String businessNumber;

    // 가맹점명
    @Column(nullable = false, length = 50)
    private String storeName;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;


    public void putStoreMemberDel() {
        this.isEnabled = false;
    }
    public void putPassword(String password) {
        this.password = password;
    }
    public void putStoreMember(StoreMemberUpdateRequest request) {
        this.dateCreate = LocalDateTime.now();
        this.name = request.getName();
        this.phoneNumber = request.getPhoneNumber();
        this.storeName = request.getStoreName();
        this.address = request.getAddress();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }




    private StoreMember(StoreMemberBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.businessNumber = builder.businessNumber;
        this.storeName = builder.storeName;
        this.address = builder.address;
        this.isEnabled = builder.isEnabled;
    }

    public static class StoreMemberBuilder implements CommonModelBuilder<StoreMember> {
        private final LocalDateTime dateCreate;
        private final MemberGroup memberGroup;
        private final String username;
        private final String password;
        private final String name;
        private final String phoneNumber;
        private final String businessNumber;
        private final String storeName;
        private final String address;
        private final Boolean isEnabled;

        // 빌더에서 회원그룹 따로 받는 이유 : 일반유저가 회원가입시 내가 일반유저다 라고 선택하지 않음.
        // 회원등록은 관리자페이지에서 관리자가 하거나 일반유저가 회원가입하거나.. N개의 경우의 수가 존재함.
        public StoreMemberBuilder(StoreMemberCreateRequest createRequest) {
            this.dateCreate = LocalDateTime.now();
            this.memberGroup = createRequest.getMemberGroup();
            this.username = createRequest.getUsername();
            this.password = createRequest.getPassword();
            this.name = createRequest.getName();
            this.phoneNumber = createRequest.getPhoneNumber();
            this.businessNumber = createRequest.getBusinessNumber();
            this.storeName = createRequest.getStoreName();
            this.address = createRequest.getAddress();
            this.isEnabled = true;
        }

        @Override
        public StoreMember build() {
            return new StoreMember(this);
        }
    }
}
