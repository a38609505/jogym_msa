package com.jogym.api.member.service;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.api.member.model.StoreMemberCreateRequest;
import com.jogym.api.member.repository.StoreMemberRepository;
import com.jogym.common.enums.MemberGroup;
import com.jogym.common.exception.CDuplicateIdExistException;
import com.jogym.common.exception.CNotMatchPasswordException;
import com.jogym.common.exception.CNotValidIdException;
import com.jogym.common.function.CommonCheck;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final StoreMemberRepository storeMemberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superadmin";
        String password = "idjh195233";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            StoreMemberCreateRequest createRequest = new StoreMemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");
            createRequest.setPhoneNumber("010-0000-0000");
            createRequest.setBusinessNumber("000-00-00000");
            createRequest.setStoreName("JO GYM");
            createRequest.setAddress("중앙동");
            createRequest.setMemberGroup(MemberGroup.ROLE_ADMIN);

            setMember(createRequest);
        }
    }


    public StoreMember setMember(StoreMemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CNotValidIdException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CNotMatchPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CDuplicateIdExistException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        StoreMember storeMember = new StoreMember.StoreMemberBuilder(createRequest).build();
        return storeMemberRepository.save(storeMember);
    }

    private boolean isNewUsername(String username) {
        long dupCount = storeMemberRepository.countByUsername(username);
        return dupCount <= 0;
    }

}
