package com.jogym.api.member.controller;

import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.model.SingleResult;
import com.jogym.api.member.model.*;
import com.jogym.common.response.service.ResponseService;
import com.jogym.api.member.service.StoreMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "가맹점 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store-member")
public class StoreMemberController {
    private final StoreMemberService storeMemberService;

    @ApiOperation(value = "가맹점 정보 등록 // 관리자")
    @PostMapping("/data")
    public CommonResult setStoreMember(@RequestBody @Valid StoreMemberCreateRequest request) {
        storeMemberService.setStoreMember(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 비밀번호 수정 // 관리자")
    @PutMapping("/password/store-member-id/{storeMemberId}")
    public CommonResult putPassword(@PathVariable long storeMemberId, @RequestBody @Valid StoreMemberUpdatePasswordRequest request) {
        storeMemberService.putPassword(storeMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 정보 리스트 // 관리자")
    @GetMapping("/list")
    public ListResult<StoreMemberItem> getStoreMember(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        return ResponseService.getListResult(storeMemberService.getStoreMember(page), true);
    }

    @ApiOperation(value = "가맹점 정보 리스트 상세보기 // 관리자")
    @GetMapping("/detail/id/{id}")
    public SingleResult<StoreMemberResponse> getStoreMemberDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(storeMemberService.getStoreMemberDetail(id));
    }

    @ApiOperation(value = "가맹점 정보 수정 // 관리자")
    @PutMapping("/store-member-id/{storeMemberId}")
    public CommonResult putStoreMember(@PathVariable long storeMemberId, @RequestBody @Valid StoreMemberUpdateRequest request) {
        storeMemberService.putStoreMember(storeMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "가맹점 정보 삭제 // 관리자")
    @PutMapping("/status/store-member-id/{storeMemberId}")
    public CommonResult putStoreMemberDel(@PathVariable long storeMemberId) {
        storeMemberService.putStoreMemberDel(storeMemberId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "id 중복확인 // 관리자")
    @GetMapping("/check/user-name")
    public SingleResult<CheckIsNewUsernameResponse> isNewUsername(@RequestParam("username") String userName){
        return ResponseService.getSingleResult(storeMemberService.isNewUsername(userName));
    }
    @ApiOperation(value = "사업자번호 중복확인 // 관리자")
    @GetMapping("/check/business-number")
    public SingleResult<CheckIsNewUsernameResponse> isNewBusinessNumber(@RequestParam("businessNumber") String businessNumber){
        return ResponseService.getSingleResult(storeMemberService.isNewBusinessNumber(businessNumber));
    }
}
