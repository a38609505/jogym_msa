package com.jogym.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StoreMemberUpdatePasswordRequest {

    @ApiModelProperty(notes = "비밀 번호(8~20)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    @ApiModelProperty(notes = "비밀 번호 확인(8~20)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;
}
