package com.jogym.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , ACCESS_DENIED(-1000, "권한이 없습니다.")
    , USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    , AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , NOT_VALID_ID(-10002, "유효한 아이디 형식이 아닙니다.")
    , NOT_MATCH_PASSWORD(-10003, "비밀번호가 일치하지 않습니다.")
    , DUPLICATE_ID_EXIST(-10004, "중복된 아이디가 존재합니다.")
    , ALREADY_USING_SEASON_TICKET(-10009, "이용 중인 정기권이 이미 존재합니다.")
    , ALREADY_DATE_CHECK_TODAY(-10013, "오늘은 이미 출석 하셨습니다.")
    , EXIST_ING_CALCULATE_HISTORY(-10014, "해당 년월에 정산 중인 내역이 이미 존재합니다.")
    , EXIST_COMPLETE_CALCULATE_HISTORY(-10015, "해당 년월은 이미 정산 완료 되었습니다.")
    , DUPLICATE_BUSINESS_NUMBER(-10016, "중복된 사업자 등록번호가 존재합니다.")
    , UNVALID_BUSINESS_NUMBER(-10017, "일치하지 않는 사업자 등록번호 형식입니다.")
    , EXPIRE_SEASON_TICKET_DATE_END(-10018, "등록하신 정기권이 만료되었습니다.")
    , EXPIRE_PT_TICKET_REMAIN_COUNT(-10019, "PT권 사용 횟수를 전부 소진하셨습니다.")
    ,
    ;
    private final Integer code;
    private final String msg;
}
