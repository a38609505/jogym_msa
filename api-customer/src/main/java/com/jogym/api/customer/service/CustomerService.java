package com.jogym.api.customer.service;

import com.jogym.api.customer.entity.Customer;
import com.jogym.api.customer.entity.StoreMember;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.customer.model.CustomerItem;
import com.jogym.api.customer.model.CustomerRequest;
import com.jogym.api.customer.model.CustomerResponse;
import com.jogym.api.customer.model.CustomerUpdateRequest;
import com.jogym.api.customer.repository.CustomerRepository;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    // 컨트롤러에서 안되서 주석처리를 해놓았음.
    // 예상하기로는 서비스에서는 문제가 없음.
    // 컨트롤러에서 해결이 되지않아 버려두고 다른거부터 하였음.
    public void setMember(StoreMember storeMember, CustomerRequest request) {
        Customer member = new Customer.MemberBuilder(storeMember, request).build();

        customerRepository.save(member);
    }

    public ListResult<CustomerItem> getMemberList(StoreMember storeMember, int page) {
        Page<Customer> originList = customerRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(storeMember, true,  ListConvertService.getPageable(page));
        List<CustomerItem> result = new LinkedList<>();

        for (Customer member : originList.getContent()) {
            result.add(new CustomerItem.Builder(member).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }


    public CustomerResponse getCustomer(long customerId, StoreMember storeMember) {
        Customer member = customerRepository.findByIdAndStoreMember(customerId,storeMember).orElseThrow(CMissingDataException::new); // 데이터가 없습니다.
        if(!member.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다.
        return new CustomerResponse.Builder(member).build();
    }

    public void putCustomer(long customerId, StoreMember storeMember, CustomerUpdateRequest request) {
        Customer originData = customerRepository.findById(customerId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다.
        if(originData.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다.
        originData.putMember(request);
        customerRepository.save(originData);
    }

    public void putCustomerDelete(long customerId, StoreMember storeMember) {
        Customer originData = customerRepository.findById(customerId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다.
        if(originData.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다.
        originData.putMemberDelete();
        customerRepository.save(originData);
    }
}
