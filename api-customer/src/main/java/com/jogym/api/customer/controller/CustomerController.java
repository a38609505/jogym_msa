package com.jogym.api.customer.controller;

import com.jogym.api.customer.entity.StoreMember;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.customer.model.CustomerItem;
import com.jogym.api.customer.model.CustomerRequest;
import com.jogym.api.customer.model.CustomerUpdateRequest;
import com.jogym.common.response.service.ResponseService;
import com.jogym.api.customer.service.CustomerService;

import com.jogym.api.customer.service.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    private final ProfileService profileService;

    @ApiOperation(value = "회원 등록 // 가맹점") // 가맹점
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid CustomerRequest request) {
        StoreMember storeMember = profileService.getMemberData();
        customerService.setMember(storeMember, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 리스트 // 가맹점") // 가맹점
    @GetMapping("/list")
    public ListResult<CustomerItem> getMembers(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getListResult(customerService.getMemberList(storeMember,page), true);
    }

    @ApiOperation(value = "회원 상세 정보 // 가맹점") // 가맹점
    @GetMapping("/{customerId}")
    public CommonResult getCustomer(@PathVariable long customerId) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getSingleResult(customerService.getCustomer(customerId, storeMember));
    }

    @ApiOperation(value = "회원 정보 수정 // 가맹점") // 가맹점
    @PutMapping("/{customerId}")
    public CommonResult putMember(@PathVariable long customerId, @RequestBody @Valid CustomerUpdateRequest request) {
        customerService.putCustomer(customerId, profileService.getMemberData(), request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 정보 삭제") // 가맹점
    @PutMapping("/customer-id/{customerId}")
    public CommonResult putMemberDelete(@PathVariable long customerId) {
        customerService.putCustomerDelete(customerId, profileService.getMemberData());
        return ResponseService.getSuccessResult();
    }
}
