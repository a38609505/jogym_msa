package com.jogym.api.customer.repository;

import com.jogym.api.customer.entity.StoreMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StoreMemberRepository extends JpaRepository<StoreMember, Long> {
    Optional<StoreMember> findByUsername(String username);

}
