package com.jogym.api.customer.model;

import com.jogym.common.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerRequest {
    @ApiModelProperty(notes = "이름(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처(13)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "주소(2-100)", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String address;

    @ApiModelProperty(notes = "성별", required = true)
    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "생년월일", required = true)
    @NotNull
    private LocalDate dateBirth;

    @ApiModelProperty(notes = "비고")
    private String memo;
}
