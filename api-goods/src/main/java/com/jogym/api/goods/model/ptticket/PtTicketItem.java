package com.jogym.api.goods.model.ptticket;

import com.jogym.api.goods.entity.PtTicket;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketItem {
    private Long id;

    private Long trainerId;

    private String ticketName;

    private Integer maxCount;

    private Double unitPrice;

    private Double totalPrice;

    private PtTicketItem(Builder builder) {
        this.id = builder.id;
        this.trainerId = builder.trainerId;
        this.ticketName = builder.ticketName;
        this.maxCount = builder.maxCount;
        this.unitPrice = builder.unitPrice;
        this.totalPrice = builder.totalPrice;
    }

    public static class Builder implements CommonModelBuilder<PtTicketItem> {

        private final Long id;

        private final Long trainerId;   // 수정하여야함.

        private final String ticketName;

        private final Integer maxCount;

        private final Double unitPrice;

        private final Double totalPrice;

        public Builder(PtTicket ptTicket) {
           this.id = ptTicket.getId();
            this.trainerId = ptTicket.getTrainer().getId();
            this.ticketName = ptTicket.getTicketName();
            this.maxCount = ptTicket.getMaxCount();
            this.unitPrice = ptTicket.getUnitPrice();
            this.totalPrice = maxCount * unitPrice;
        }
        @Override
        public PtTicketItem build() {
            return new PtTicketItem(this);
        }
    }

}
