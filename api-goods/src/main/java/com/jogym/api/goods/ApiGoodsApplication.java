package com.jogym.api.goods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGoodsApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiGoodsApplication.class, args);
    }
}
