package com.jogym.api.goods.model.seasonticket;

import com.jogym.common.enums.TicketType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SeasonTicketUpdateRequest {

    @ApiModelProperty(notes = "정기권명", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String ticketName;

    @ApiModelProperty(notes = "최대 월", required = true)
    @NotNull
    @Min(0)
    private Integer maxMonth;

    @ApiModelProperty(notes = "요금", required = true)
    @NotNull
    private Double unitPrice;
}
