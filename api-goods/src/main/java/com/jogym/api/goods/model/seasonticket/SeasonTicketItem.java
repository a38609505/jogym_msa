package com.jogym.api.goods.model.seasonticket;

import com.jogym.api.goods.entity.SeasonTicket;
import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicketItem {
    private Long id;

    private String ticketType;

    private String ticketName;

    private Integer maxMonth;

    private Double unitPrice;

    private Double totalPrice;

    private SeasonTicketItem(Builder builder) {
        this.id = builder.id;
        this.ticketType = builder.ticketType;
        this.ticketName = builder.ticketName;
        this.maxMonth = builder.maxMonth;
        this.unitPrice = builder.unitPrice;
        this.totalPrice = builder.totalPrice;
    }

    public static class Builder implements CommonModelBuilder<SeasonTicketItem> {

        private final Long id;

        private final String ticketType;

        private final String ticketName;

        private final Integer maxMonth;

        private final Double unitPrice;

        private final Double totalPrice;

        public Builder(SeasonTicket seasonTicket) {
            this.id = seasonTicket.getId();
            this.ticketType = seasonTicket.getTicketType().getName();
            this.ticketName = seasonTicket.getTicketName();
            this.maxMonth = seasonTicket.getMaxMonth();
            this.unitPrice = seasonTicket.getUnitPrice();
            this.totalPrice = maxMonth * unitPrice;
        }

        @Override
        public SeasonTicketItem build() {
            return new SeasonTicketItem(this);
        }
    }

}
