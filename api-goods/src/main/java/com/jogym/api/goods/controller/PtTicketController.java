package com.jogym.api.goods.controller;

import com.jogym.api.goods.entity.StoreMember;
import com.jogym.api.goods.entity.Trainer;
import com.jogym.api.goods.service.ProfileService;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.goods.model.ptticket.PtTicketItem;
import com.jogym.api.goods.model.ptticket.PtTicketRequest;
import com.jogym.api.goods.model.ptticket.PtTicketUpdateRequest;
import com.jogym.common.response.service.ResponseService;
import com.jogym.api.goods.service.PtTicketService;
import com.jogym.api.goods.service.TrainerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "PT권 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pt-ticket")
public class PtTicketController {
    private final PtTicketService ptTicketService;
    private final TrainerService trainerService;
    private final ProfileService profileService;


    @ApiOperation(value = "PT권 등록 // 가맹점")
    @PostMapping("/trainer-id/{trainerId}")
    public CommonResult setPtTicket(@PathVariable long trainerId, @RequestBody @Valid PtTicketRequest request) {
        // 변수값은 PathVariable로, Request는 RequestBody로 받고 Body를 사용할 땐 알아서 검사해주는 Valid가 따라온다.

        StoreMember storeMember = profileService.getMemberData();
        // token 건네주고 원본을 받아오는 과정

        Trainer trainerMember = trainerService.getOriginTrainer(trainerId);
        // id를 건네주고 원본을 받아오는 과정

        ptTicketService.setPtTicket(storeMember, trainerMember, request);
        // 건네받은 아이디와 준비한 자료들을 사용하여 기능을 수행시키는 과정

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult을 주어 성공했다는 문구를 남겨준다.
    }

    @ApiOperation(value = "PT권 리스트 // 가맹점")
    @GetMapping("/list")
    public ListResult<PtTicketItem> getPtTicket(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        // List에서 사용할 도구명을 작성, RequestParam을 사용해 value값에 page를 넣어주고 필수임을 알려주며 page의 타입은 int라는 것을 알려준다.

        StoreMember storeMember = profileService.getMemberData();
        // token 건네주고 원본을 받아오는 과정

        return ResponseService.getListResult(ptTicketService.getPtTicket(storeMember, page), true);
        // return으로 getListResult을 주고 어느 곳의 기능을 사용할 것인 지 선택하는 과정
    }

    @ApiOperation(value = "PT권 수정 // 가맹점")
    @PutMapping("/{ptTicketId}")
    public CommonResult putPtTicket(@PathVariable long ptTicketId, @RequestBody @Valid PtTicketUpdateRequest request) {
        // 기능을 수행하기 위해 자료를 준비하는 과정

        ptTicketService.putPtTicet(ptTicketId, request);
        // 준비한 자료들을 넣어 기능을 수행하는 과정

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult를 주어 성공했다는 문구를 남겨주는 과정
    }

    @ApiOperation(value = "PT권 삭제 // 가맹점")
    @PutMapping("/pt-ticket-id/{ptTicketId}")
    public CommonResult putPtTicket(@PathVariable long ptTicketId) {
        // 기능을 수행하기 위해 자료를 준비하는 과정

        ptTicketService.putPtTicketDelete(ptTicketId);
        // 준비한 자료들을 기능 수행을 위해 사용하는 과정

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult를 주어 성공했다는 문구를 남겨주는 과정
    }
}
