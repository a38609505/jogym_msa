#!/bin/bash

# variables
gcpArea="asia.gcr.io"
projectId="windy-lyceum-398100"
gcpRegion="asia-northeast3"
gkeClusterName="jogym-cluster"
dockerImage="api-trainer"

# script
docker login -u _json_key --password-stdin https://asia.gcr.io < key.json
docker build -t ${dockerImage} .
docker tag ${dockerImage} "${gcpArea}/${projectId}/${dockerImage}"
docker push "${gcpArea}/${projectId}/${dockerImage}:latest"

#gcloud auth activate-service-account --key-file key.json
export USE_GKE_GCLOUD_AUTH_PLUGIN=Ture
gcloud container clusters get-credentials ${gkeClusterName} --region ${gcpRegion} --projectId ${projectId}
kubectl delete deployment ${dockerImage}
kubectl apply -f k8s-deployment.yaml
#kubectl apply -f k8s-service.yaml
#kubectl apply -f k8s-ingress.yaml