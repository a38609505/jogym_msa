package com.jogym.api.use.repository;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.PtTicketUsedHistory;
import com.jogym.api.use.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketUsedHistoryRepository extends JpaRepository<PtTicketUsedHistory, Long> {
    Page<PtTicketUsedHistory> findAllByPtTicketBuyHistory_Customer_StoreMemberAndPtTicketBuyHistory_Customer(StoreMember storeMember, Customer customer, Pageable pageable);
}
