package com.jogym.api.use.repository;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface SeasonTicketBuyHistoryRepository extends JpaRepository<SeasonTicketBuyHistory, Long> {
    Optional<SeasonTicketBuyHistory> findByCustomerAndDateLast(Customer customer, LocalDate dateLast);
}
