package com.jogym.api.use.repository;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketUsedHistory;
import com.jogym.api.use.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SeasonTicketUsedHistoryRepository extends JpaRepository<SeasonTicketUsedHistory, Long> {
    Page<SeasonTicketUsedHistory> findAllByIdGreaterThanEqualAndSeasonTicketBuyHistory_CustomerAndSeasonTicketBuyHistory_Customer_StoreMemberOrderByIdDesc(long id, Customer customer, StoreMember storeMember, Pageable pageable);
}
