package com.jogym.api.use.model.seasonticketusedhistory;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketUsedHistory;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.common.function.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicketUsedHistoryItem {
    private Long id;

    private Long customerId;

    private String dateCreate;

    private String ticketName;

    private SeasonTicketUsedHistoryItem(Builder builder) {
        this.id = builder.id;
        this.customerId = builder.customerId;
        this.dateCreate = builder.dateCreate;
        this.ticketName = builder.ticketName;
    }

    public static class Builder implements CommonModelBuilder<SeasonTicketUsedHistoryItem> {

        private final Long id;

        private final Long customerId;

        private final String dateCreate;

        private final String ticketName;

        public Builder(Customer customer, SeasonTicketUsedHistory seasonTicketUsedHistory) {
            this.id = seasonTicketUsedHistory.getId();
            this.customerId = customer.getId();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(seasonTicketUsedHistory.getDateCreate());
            this.ticketName = seasonTicketUsedHistory.getSeasonTicketBuyHistory().getSeasonTicket().getTicketName();
        }
        @Override
        public SeasonTicketUsedHistoryItem build() {
            return new SeasonTicketUsedHistoryItem(this);
        }
    }
}
