package com.jogym.api.use.entity;

import com.jogym.common.enums.Gender;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeMemberId", nullable = false)
    private StoreMember storeMember;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 연락처
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 성별
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

}
