package com.jogym.api.use.service;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import com.jogym.api.use.entity.SeasonTicketUsedHistory;
import com.jogym.api.use.entity.StoreMember;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.use.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.api.use.repository.SeasonTicketUsedHistoryRepository;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SeasonTicketUsedHistoryService {
    private final SeasonTicketUsedHistoryRepository seasonTicketUsedHistoryRepository;

    public void setSeasonTicketUsedHistory(SeasonTicketBuyHistory seasonTicketBuyHistory) {
        SeasonTicketUsedHistory seasonTicketUsedHistory = new SeasonTicketUsedHistory.SeasonTicketUsedHistoryBuilder(seasonTicketBuyHistory).build();

        seasonTicketUsedHistoryRepository.save(seasonTicketUsedHistory);
    }

    public ListResult<SeasonTicketUsedHistoryItem> getData(int page, Customer customer, StoreMember storeMember) {
        if (!customer.getStoreMember().equals(storeMember)) throw new CMissingDataException();
        Page<SeasonTicketUsedHistory> originList =
                seasonTicketUsedHistoryRepository.findAllByIdGreaterThanEqualAndSeasonTicketBuyHistory_CustomerAndSeasonTicketBuyHistory_Customer_StoreMemberOrderByIdDesc(1, customer, storeMember ,ListConvertService.getPageable(page));
        List<SeasonTicketUsedHistoryItem> result = new LinkedList<>();
        for (SeasonTicketUsedHistory seasonTicketUsedHistory : originList) {
            result.add(new SeasonTicketUsedHistoryItem.Builder(customer, seasonTicketUsedHistory).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public void isCurrentStoreMember(StoreMember storeMember, SeasonTicketBuyHistory seasonTicketBuyHistory){
        if (storeMember != seasonTicketBuyHistory.getSeasonTicket().getStoreMember())
            throw new CMissingDataException();
    }

}
