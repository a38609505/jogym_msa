package com.jogym.api.use.service;

import com.jogym.api.use.entity.PtTicketBuyHistory;
import com.jogym.api.use.repository.PtTicketBuyHistoryRepository;
import com.jogym.common.exception.CExpirePtTicketRemainCountException;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PtTicketBuyHistoryService {
    private final PtTicketBuyHistoryRepository ptTicketBuyHistoryRepository;

    public PtTicketBuyHistory getData(long ptTicketBuyHistoryId){
        return ptTicketBuyHistoryRepository.findById(ptTicketBuyHistoryId).orElseThrow(CMissingDataException::new);
    }

    public void isExpirePtTicket(PtTicketBuyHistory ptTicketBuyHistory){
        if (ptTicketBuyHistory.getRemainCount() < 1){
            ptTicketBuyHistory.expireBuyStatus();
            ptTicketBuyHistoryRepository.save(ptTicketBuyHistory);
            throw new CExpirePtTicketRemainCountException();
        } else {
            ptTicketBuyHistory.putUseCount();
            ptTicketBuyHistoryRepository.save(ptTicketBuyHistory);
        }
    }
}
