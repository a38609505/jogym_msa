package com.jogym.api.use.entity;

import com.jogym.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketUsedHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // PT권 구매내역 id
    @JoinColumn(name = "ptTicketBuyHistoryId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PtTicketBuyHistory ptTicketBuyHistory;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 사용횟수
    @Column(nullable = false)
    private Integer usedCount;

    // 잔여횟수
    @Column(nullable = false)
    private Integer remainCount;

    private PtTicketUsedHistory(PtTicketUsedHistoryBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.ptTicketBuyHistory = builder.ptTicketBuyHistory;
        this.maxCount = builder.maxCount;
        this.usedCount = builder.usedCount;
        this.remainCount = builder.remainCount;
    }

    public static class PtTicketUsedHistoryBuilder implements CommonModelBuilder<PtTicketUsedHistory> {

        private final LocalDateTime dateCreate;
        private final PtTicketBuyHistory ptTicketBuyHistory;
        private final Integer maxCount;
        private final Integer usedCount;
        private final Integer remainCount;

        public PtTicketUsedHistoryBuilder(PtTicketBuyHistory ptTicketBuyHistory) {
            this.dateCreate = LocalDateTime.now();
            this.ptTicketBuyHistory = ptTicketBuyHistory;
            this.maxCount = ptTicketBuyHistory.getMaxCount();
            this.usedCount = ptTicketBuyHistory.getUsedCount();
            this.remainCount = ptTicketBuyHistory.getRemainCount();
        }


        @Override
        public PtTicketUsedHistory build() {
            return new PtTicketUsedHistory(this);
        }
    }
}
