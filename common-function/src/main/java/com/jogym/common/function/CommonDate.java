package com.jogym.common.function;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;

public class CommonDate {
    public static LocalDate getNowDate() {
        LocalDateTime nowTime = LocalDateTime.now().plusHours(9);
        return LocalDate.of(nowTime.getYear(), nowTime.getMonth(), nowTime.getDayOfMonth());
    }

    public static LocalDateTime getNowTime() {
        return LocalDateTime.now().plusHours(9);
    }

    public static int getLastDay(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, 1);

        int lastDay = cal.getActualMaximum(Calendar.DATE);

        return lastDay;
    }

}
